export interface Problem {
  type?: string;
  title: string;
  status: number;
  detail: string;
}

export interface Violation {
  field: string;
  message: string;
}

export interface ConstraintViolation extends Problem {
  violations: Violation[];
}

export function isProblem(object: unknown): object is Problem {
  return (
    typeof object === "object" &&
    object != null &&
    "status" in object &&
    "title" in object
  );
}

export function isConstraintViolation(
  object: unknown
): object is ConstraintViolation {
  return (
    isProblem(object) &&
    object.type === "https://zalando.github.io/problem/constraint-violation"
  );
}
