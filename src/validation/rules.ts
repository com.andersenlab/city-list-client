export class InputLength {
  constructor(
    private max: number,
    private message = "Text size should not exceed #max symbols",
    private trigger = ["blur", "change"]
  ) {
    this.message = message.replace("#max", String(max));
  }
}

export const httpLinkPattern = new RegExp("^(http|https|file)://.*");

export const httpLinkFormat = {
  pattern: httpLinkPattern,
  trigger: ["blur", "change"],
  message: "Incorrect link format. It should start from http or https",
};

export const inputRequired = {
  message: "Please select a value",
  required: true,
  pattern: /\S+/,
  trigger: ["blur", "change"],
};
