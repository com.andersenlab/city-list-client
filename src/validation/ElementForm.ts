import type { ElForm } from "element-plus";

export type ElementForm = InstanceType<typeof ElForm>;
