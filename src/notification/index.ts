import { error, info } from "@/notification/notification";

export const notifications = {
  info,
  error,
};
