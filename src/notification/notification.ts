import { ElNotification } from "element-plus";

export function info(message: string): void {
  ElNotification({
    type: "info",
    title: "info",
    message: message,
    position: "bottom-right",
    offset: 50,
  });
}

export function error(message: string): void {
  ElNotification({
    type: "error",
    title: "error",
    message: message,
    position: "bottom-right",
    offset: 50,
  });
}
