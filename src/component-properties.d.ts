import { notifications } from "@/notification";

declare module "@vue/runtime-core" {
  export interface ComponentCustomProperties {
    $notify: typeof notifications;
  }
}
